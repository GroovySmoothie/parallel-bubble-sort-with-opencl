#ifdef __APPLE__
#include <OpenCL/opencl.hpp>
#else
#include <CL/cl.hpp>
#endif

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <climits>

#define DEFAULT_PROBLEM_SIZE 5000

// Checks whether an array is sorted.
// inputs: int *array - Pointer to the array of numbers.
//         int length - length of array.
// output: True if the array was sorted. False otherwise.
bool sorted(int *array, int length)
{
	int i = 0;
	for (i = 0; i < length - 1; i++)
	{
		if (array[i] > array[i + 1])
		{
			return false;
		}
	}
	return true;
}

// Load kernel code from a file into a string.
// inputs: const char* filename - filename of kernel code.
// output: string - kernel code.
std::string loadKernel(const char *filename)
{
	std::ifstream f(filename);
	// Read the file a string stream buffer and convert it to a string.
	std::stringstream buffer;
	buffer << f.rdbuf();
	f.close();

	return buffer.str();
}

// Inputs: N - length of array to sort.
int main(int argc, char **argv)
{
	// Set default size.
	int N = DEFAULT_PROBLEM_SIZE;
	// Parse array size N from input if provided.
	if (argc == 2)
	{
		std::istringstream(argv[1]) >> N;
	}
	else
	{
		std::cout << "Program requires one input: N - size of array to sort." << std::endl;
		exit(1);
	}

	// Generate a random array with the given length.
	int *array = new int[N];

	srand(time(NULL));
	for (int i = 0; i < N; i++)
	{
		array[i] = rand() + rand() - RAND_MAX;
	}

	// Error code variable for checking success of each openCL function.
	cl_int err;

	// Initialises vector to get a list of available platforms
	// uses a cl::Platform::get to populate the vector and saves the return value
	// Returns an error if no platforms are found
	// Otherwise chooses the default platform, Nvidia most likely
	std::vector<cl::Platform> all_platforms;
	err = cl::Platform::get(&all_platforms);
	if (all_platforms.size() == 0 && err != CL_SUCCESS)
	{
		std::cout << "No platforms found! Error code: " << err << std::endl;
		exit(1);
	}
	cl::Platform default_platform = all_platforms[0];

	// Same as platforms, populates a vector of all decives on your default platform
	// Returns error if no devices, otherwise gets the default device, GPU
	std::vector<cl::Device> all_devices;
	err = default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
	if (all_devices.size() == 0 && err != CL_SUCCESS)
	{
		std::cout << "No devices found! Error code: " << err << std::endl;
	}
	cl::Device default_device = all_devices[0];

	// The context is like a reference we can use to connect to the platform and device
	// We create this context from the deafult platform and device found above
	cl::Context context(default_device);

	// Initializing a command queue that will hold all of the command we want to send to the device
	cl::CommandQueue queue(context, default_device, CL_QUEUE_PROFILING_ENABLE, &err);
	if (err != CL_SUCCESS)
	{
		std::cout << "Could not make command queue, error: " << err << std::endl;
		exit(1);
	}

	// Initializing special cl buffers to be sent to the device
	// buffer_A holds the array to be sorted
	// buffer_B holds the size of the array, N
	cl::Buffer buffer_A(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(int) * N, array, &err);
	cl::Buffer buffer_B(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(int), &N, &err);

	// Sources contains the source code for all the kernels
	// The source code is loaded from the file kernel_code.cl
	// The source code is converted into a c string
	// Then pushed back into sources
	cl::Program::Sources sources;
	std::string kernel_code = loadKernel("kernel_code.cl");
	sources.push_back({kernel_code.c_str(), kernel_code.length()});

	// Constructs a program from the kernals in sources given a context
	cl::Program program(context, sources);

	// Builds the program for the default device and captures any errors
	err = program.build({default_device});
	if (err != CL_SUCCESS)
	{
		std::cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << " Error code: " << err << std::endl;
		exit(1);
	}

	// Calculate the optimal global workgroup size for the given problem size.
	// Ensures that the global workgroup size doesn't default to 1 (sequential program) for specific problem sizes by padding extra processors.
	// Original code by Marco13 from: https://stackoverflow.com/questions/22968369/get-optimum-local-global-workgroup-size-in-opencl/22969485#22969485
	// Get the maximum work group size for the default device.
	size_t workgroup_size;
	err = clGetDeviceInfo(default_device(), CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(workgroup_size), &workgroup_size, NULL);
	if (err != CL_SUCCESS)
	{
		std::cout << workgroup_size << std::endl;
		std::cout << "Error: " << err << std::endl;
		exit(1);
	}
	int maxWorkGroupSize = workgroup_size;
	int numWorkGroups = (N - 1) / maxWorkGroupSize + 1;
	int globalSizePadded = numWorkGroups * maxWorkGroupSize;

	// Sets up two kernels
	// In the source code loaded earlier there are two kernels
	// Even step and odd step for the parallel bubble sort
	cl::Kernel kernel_even_step = cl::Kernel(program, "even_step");
	cl::Kernel kernel_odd_step = cl::Kernel(program, "odd_step");

	// Sets the arguments for both kernels
	// We use the buffers from earlier to pass the array and array size as arguments
	kernel_even_step.setArg(0, buffer_A);
	kernel_even_step.setArg(1, buffer_B);
	kernel_odd_step.setArg(0, buffer_A);
	kernel_odd_step.setArg(1, buffer_B);

	/* Perform parallel bubble sort. */
	cl_ulong time_start = 0, time_end = 0;
	cl_ulong total_time = 0;
	cl::Event event;

	// The odd and even steps need to run N times across all elements in total
	// This guarantees a sorted list
	for (int k = 0; k < N; k++)
	{
		// The odd and even steps need to be alternated so a modulo is used
		// We enqueue the kernel onto the device to be run, the parameters are as follows:
		// Kernel& kernel : which has the code to be run
		// NDRange& offset : used to offset the global id of a work item, NullRange sets no offset
		// NDRange& global : an array to define how many work items in each work dimension
		// NDRange& local : an array to define how many work items make up a work group, NullRange will let the OpenCL implementation handle this
		// VECTOR_CLASS<Event> * events : A vector of events that need to run before this kernel, NULL means it will run immediately
		// Event * event : Used to wait for the kernel to finish running and also to query running time
		if (k % 2 == 0)
		{
			queue.enqueueNDRangeKernel(kernel_even_step, cl::NullRange, globalSizePadded, cl::NullRange, NULL, &event);
		}
		else
		{
			queue.enqueueNDRangeKernel(kernel_odd_step, cl::NullRange, globalSizePadded, cl::NullRange, NULL, &event);
		}

		// Wait for queued kernel to finish execution.
		event.wait();
		// Get execution time for the kernel and add it to the total time.
		time_start = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
		time_end = event.getProfilingInfo<CL_PROFILING_COMMAND_END>();
		total_time += time_end - time_start;
	}

	// Initializes an array to store the result from the device
	// Adds a read instruction to the queue that will read from the buffer holding the array on the device
	int *res = new int[N];
	queue.enqueueReadBuffer(buffer_A, CL_TRUE, 0, sizeof(int) * N, res);

	// If the array is sorted, output the execution time.
	if (sorted(res, N))
	{
		std::cout << 1.0 * total_time / 1000000000 << std::endl;
	}
	else
	{
		std::cout << "Error: array not sorted after kernel execution." << std::endl;
		exit(1);
	}

	// Clean up.
	delete array;
	delete res;

	return 0;
}
