void kernel even_step(global int* A, global const int* len) {
	int id = get_global_id(0);
	// setting indexes for left and right elements in partition
	int left = id*2;
	int right = id*2+1;
	// checking if index is out of bounds
	if(right < *len) {
		// performing swap if out of order
		if(A[left] > A[right]) {
			int temp = A[left];
			A[left] = A[right];
			A[right] = temp;
		}
	}
}

void kernel odd_step(global int* A, global const int* len) {
	int id = get_global_id(0);
	// setting indexes for left and right elements in partition
	int left = id*2+1;
	int right = id*2+2;
	// checking if index is out of bounds
	if(right < *len) {
		// performing swap if out of order
		if(A[left] > A[right]) {
			int temp = A[left];
			A[left] = A[right];
			A[right] = temp;
		}
	}
}
